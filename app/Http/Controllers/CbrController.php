<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Services\CbrService;
use App\Http\Requests\CbrRequest;

class CbrController extends Controller
{
    public function index() {
        return view('welcome');
    }

    public function show(CbrRequest $request, CbrService $service) {
        list('day' => $day, 'month' => $month, 'year' => $year) = $request->validated();
        $carbon = Carbon::createFromDate($year,$month,$day);
        $date = $carbon->format('d.m.Y');
        $subDate = $carbon->subDay(1)->format('d.m.Y');
        if (!$service->load($date)) {
            redirect()->route('index');
        }
        $usd_now = $service->get('USD');
        $eur_now = $service->get('EUR');
        $service->load($subDate);
        $eur_sub = $service->get('EUR') ?? $eur_now;
        $usd_sub = $service->get('USD') ?? $usd_now;
        $icon = $usd_now < $usd_sub ? 'down' : 'up';
        $icon_eur = $eur_now < $eur_sub ? 'down' : 'up';
      //  dd($usd_now, $usd_sub);
        return view('show', compact('date','usd_now','icon', 'eur_now', 'icon_eur'));
    }
}
