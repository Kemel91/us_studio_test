<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="card" style="width: 40rem; margin: 20%">
            <div class="card-body">
                <h5 class="card-title">Парсер валют с сайта cbr.ru</h5>
                <h6 class="card-subtitle mb-2 text-muted">Курс на {{ $date }}:</h6>
                <p class="card-text">
                    Курс доллара : {{  $usd_now }}
                    <img src="{{ asset('icons/'.$icon.'.png') }}">
                    Курс евро : {{  $eur_now }}
                    <img src="{{ asset('icons/'.$icon_eur.'.png') }}">
                </p>
                <a href="{{ route('index') }}" class="card-link">Назад</a>
            </div>
        </div>
    </div>

</div>
</body>
</html>

