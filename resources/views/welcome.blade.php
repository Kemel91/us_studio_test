<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
    <div class="card" style="width: 40rem; margin: 20%">
        <div class="card-body">
            <h5 class="card-title">Парсер валют с сайта cbr.ru</h5>
            <h6 class="card-subtitle mb-2 text-muted">Выберите дату:</h6>

            <form action="{{ route('index.show') }}" method="post">
                @csrf
                <p class="card-text">
                <div class="row">
                    <div class="col">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Число</label>
                            </div>
                            <select name="day" class="custom-select" id="inputGroupSelect01">
                                @for($i=1; $i <= 31; $i++)
                                    <option {{ $i == date('d') ? 'selected' : '' }} value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Месяц</label>
                            </div>
                            <select name="month" class="custom-select" id="inputGroupSelect02">
                                @for($i=1; $i<=12; $i++)
                                    <option {{ $i == date('m') ? 'selected' : '' }} value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Год</label>
                            </div>
                            <select name="year" class="custom-select" id="inputGroupSelect03">
                                @for($i = 2000; $i <= date('Y'); $i++)
                                    <option {{ $i == date('Y') ? 'selected' : '' }} value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <button type="submit" class="btn btn-danger" style="margin-top: 2em;">Выбрать</button>
                </div>
                </p>
            </form>
        </div>
    </div>
    </div>

</div>
</body>
</html>
